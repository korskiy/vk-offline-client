<?php
namespace app\vk;

class Loader
{
    private static $instance;
    private function __construct() {}

    public static function instance()
    {
        if (empty(self::$instance)) {
            self::$instance = new Loader();
        }
        return self::$instance;
    }

    public function download($url, $data, $method = 'POST')
    {

        $data = http_build_query($data);
        $url .= $data;

        $opts = array(
            'http' => array(
                'method' => "GET",
                'header' => "Accept-language: ru\r\n" .
                    "Content-Type: text/html; charset=utf-8"
            )
        );

        $context = stream_context_create($opts);

        /*$ch = curl_init();

        if ($method === 'POST') {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        } else {
            $url .= $data;
        }
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        $response = curl_exec($ch);
        // $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);*/
        $response = file_get_contents($url, false, $context);
        return $response;
    }

    public function get($url, $data)
    {
        return $this->download($url, $data, 'GET');
    }

    /**
     * ���������� ������ ���� �������
     * @param string $url
     * @param array $data
     * @return string
     */
    public function post($url, $data)
    {
        return $this->download($url, $data);
    }
}
